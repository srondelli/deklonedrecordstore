/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.monejava.dekloned.discogs;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Properties;

import nl.monejava.dekloned.discogs.entities.DiscogsRelease;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * 
 * @author Simone Rondelli - mone.java[at]gmail.com
 */
public class DiscogsService {

    private static final String DISCOGS_URL = "http://api.discogs.com/releases/";

    private final OAuthService service;

    private final Token access_token;

    public DiscogsService() throws IOException {
        Properties configProps = new Properties();

        try (FileInputStream fis = new FileInputStream("config.properties")) {
            configProps.load(fis);
        }

        String consumer_key = configProps.getProperty("consumer.key");
        String consumer_secret = configProps.getProperty("consumer.secret");
        String user_agent_string = configProps.getProperty("useragent.string");

        // will be empty on the first run, as token is not yet provided
        String config_token = configProps.getProperty("accesstoken.key", "");
        String config_secret = configProps.getProperty("accesstoken.secret", "");

        // set the user-agent system property, will be used by modified
        // scibe-java
        // in OAuth10aServiceImpl to set user-agent header
        System.setProperty("DiscoGS-User-Agent", user_agent_string);

        service = new ServiceBuilder().provider(DiscoGSApi10a.class).apiKey(consumer_key)
                .apiSecret(consumer_secret).debug().build();

        if (("".equals(config_token)) || ("".equals(config_secret))) {
            Token request_token = service.getRequestToken();
            String authUrl = service.getAuthorizationUrl(request_token);

            String code = getDiscoGSAuthCodeFromCommandline(authUrl);

            Verifier v = new Verifier(code);
            access_token = service.getAccessToken(request_token, v);

            // save access token to config.properties
            configProps.setProperty("accesstoken.key", access_token.getToken());
            configProps.setProperty("accesstoken.secret", access_token.getSecret());

            try (PrintWriter writer = new PrintWriter("config.properties")) {
                configProps.store(writer, "Modified by DiscoGSOAuthExample");
            }
        }
        else {
            access_token = new Token(config_token, config_secret);
        }
    }

    public DiscogsRelease downloadRelease(String releaseId) {
        // this requires user interaction if no access token is found in
        // config.properties
        // do we already have an access token or do we have to authenticate?
        // ask for your identity
        OAuthRequest request = new OAuthRequest(Verb.GET, DISCOGS_URL + releaseId);
        service.signRequest(access_token, request);
        Response response = request.send();

        DiscogsRelease release = new Gson().fromJson(response.getBody(), DiscogsRelease.class);

        if (release.getId() == null) {
            return null;
        }

        return release;
    }

    public String serialize(DiscogsRelease release) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(release);
    }

    public static String getDiscoGSAuthCodeFromCommandline(String authUrl) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String code = null;
        System.out.println("");
        System.out.println(">> Use a browser and navigate to: ");
        System.out.println(">> " + authUrl);
        System.out.println(">> to authorize this application for usage of your discogs-account");
        System.out.println("");
        System.out.println(">> Enter the authorization code and press return:");
        System.out.print(">> ");
        try {
            code = br.readLine();
        }
        catch (IOException ioe) {
            System.err.println("IO error while reading your authorization code: "
                    + ioe.getMessage());
        }
        return code;
    }

}
