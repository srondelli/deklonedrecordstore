
package nl.monejava.dekloned.discogs.entities;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DiscogsRelease {

    @Expose
    private List<String> styles = new ArrayList<String>();
    @Expose
    private List<Video> videos = new ArrayList<Video>();
    @Expose
    private List<Object> series = new ArrayList<Object>();
    @Expose
    private List<Label> labels = new ArrayList<Label>();
    @Expose
    private Integer year;
    @Expose
    private Community community;
    @Expose
    private List<Artist> artists = new ArrayList<Artist>();
    @Expose
    private List<Image> images = new ArrayList<Image>();
    @SerializedName("format_quantity")
    @Expose
    private Integer formatQuantity;
    @Expose
    private Integer id;
    @Expose
    private List<String> genres = new ArrayList<String>();
    @Expose
    private String thumb;
    @Expose
    private List<Object> extraartists = new ArrayList<Object>();
    @Expose
    private String title;
    @SerializedName("date_changed")
    @Expose
    private String dateChanged;
    @Expose
    private List<Tracklist> tracklist = new ArrayList<Tracklist>();
    @Expose
    private String status;
    @SerializedName("released_formatted")
    @Expose
    private String releasedFormatted;
    @SerializedName("estimated_weight")
    @Expose
    private Integer estimatedWeight;
    @Expose
    private String released;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;
    @Expose
    private String country;
    @Expose
    private List<Object> companies = new ArrayList<Object>();
    @Expose
    private String uri;
    @Expose
    private List<Format> formats = new ArrayList<Format>();
    @SerializedName("resource_url")
    @Expose
    private String resourceUrl;
    @SerializedName("data_quality")
    @Expose
    private String dataQuality;

    public List<String> getStyles() {
        return styles;
    }

    public void setStyles(List<String> styles) {
        this.styles = styles;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<Object> getSeries() {
        return series;
    }

    public void setSeries(List<Object> series) {
        this.series = series;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Community getCommunity() {
        return community;
    }

    public void setCommunity(Community community) {
        this.community = community;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Integer getFormatQuantity() {
        return formatQuantity;
    }

    public void setFormatQuantity(Integer formatQuantity) {
        this.formatQuantity = formatQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public List<Object> getExtraartists() {
        return extraartists;
    }

    public void setExtraartists(List<Object> extraartists) {
        this.extraartists = extraartists;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged;
    }

    public List<Tracklist> getTracklist() {
        return tracklist;
    }

    public void setTracklist(List<Tracklist> tracklist) {
        this.tracklist = tracklist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReleasedFormatted() {
        return releasedFormatted;
    }

    public void setReleasedFormatted(String releasedFormatted) {
        this.releasedFormatted = releasedFormatted;
    }

    public Integer getEstimatedWeight() {
        return estimatedWeight;
    }

    public void setEstimatedWeight(Integer estimatedWeight) {
        this.estimatedWeight = estimatedWeight;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Object> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Object> companies) {
        this.companies = companies;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<Format> getFormats() {
        return formats;
    }

    public void setFormats(List<Format> formats) {
        this.formats = formats;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getDataQuality() {
        return dataQuality;
    }

    public void setDataQuality(String dataQuality) {
        this.dataQuality = dataQuality;
    }

}
