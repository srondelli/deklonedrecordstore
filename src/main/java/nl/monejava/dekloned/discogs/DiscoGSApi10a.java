/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.monejava.dekloned.discogs;

import java.io.IOException;
import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

/*
 * @author MrDevJay 2014
 */
public class DiscoGSApi10a extends DefaultApi10a {

    private final String AUTHORIZE_URL = "http://www.discogs.com/oauth/authorize?oauth_token=%s";

    private final String REQUEST_TOKEN_URL = "http://api.discogs.com/oauth/request_token";

    private final String ACCESS_TOKEN_URL = "http://api.discogs.com/oauth/access_token";

    public DiscoGSApi10a() throws IOException {

    }

    @Override
    public String getAccessTokenEndpoint() {
        return this.ACCESS_TOKEN_URL;
    }

    @Override
    public String getAuthorizationUrl(Token request_token) {
        return String.format(this.AUTHORIZE_URL, request_token.getToken());
    }

    @Override
    public String getRequestTokenEndpoint() {
        return this.REQUEST_TOKEN_URL;
    }

}
