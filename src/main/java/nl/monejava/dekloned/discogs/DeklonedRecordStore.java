/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.monejava.dekloned.discogs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import nl.monejava.dekloned.discogs.entities.DiscogsRelease;
import nl.monejava.dekloned.discogs.util.CSVExporter;
import nl.monejava.dekloned.discogs.util.DeklonedUtils;
import org.controlsfx.dialog.Dialogs;

/**
 *
 * @author Simone Rondelli <mone.java@gmail.com>
 */
public class DeklonedRecordStore extends Application {

    private Stage stage;

    @Override
    public void start(final Stage primaryStage) {
        this.stage = primaryStage;
        primaryStage.setTitle("Dekloned Record Store");
        Group root = new Group();
        root.setAutoSizeChildren(true);
        final Scene scene = new Scene(root, Color.WHITE);

        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        root.getChildren().add(gridpane);

        final TextArea textArea = new TextArea("25826");
        textArea.setPrefRowCount(30);
        textArea.setPrefColumnCount(100);
        textArea.setWrapText(true);
        textArea.setPrefWidth(300);
        GridPane.setHalignment(textArea, HPos.CENTER);
        gridpane.add(textArea, 0, 1);

        BorderPane savePathPane = new BorderPane();

        final TextField pathFld = new TextField(System.getProperty("user.home") + File.separator + "dekloned.csv");
        savePathPane.setCenter(pathFld);

        Button openFileBtn = new Button("...");
        openFileBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Select CSV exported file destination");
                File f = fileChooser.showSaveDialog(primaryStage);

                if (f != null) {
                    pathFld.setText(f.getAbsolutePath());
                }
            }
        });
        savePathPane.setRight(openFileBtn);
        gridpane.add(savePathPane, 0, 2);

        Button downloadBtn = new Button();
        downloadBtn.setText("Download");
        downloadBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String text = textArea.getText();
                StringTokenizer tokenizer = new StringTokenizer(text);
                List<String> strings = new ArrayList<>(tokenizer.countTokens());

                while (tokenizer.hasMoreTokens()) {
                    strings.add(tokenizer.nextToken());
                }

                try {
                    scene.setCursor(Cursor.WAIT);
                    download(strings, pathFld.getText());
                } finally {
                    scene.setCursor(Cursor.DEFAULT);
                }
            }
        });
        gridpane.add(downloadBtn, 0, 3);

        GridPane.setHalignment(downloadBtn, HPos.CENTER);

        primaryStage.setScene(scene);
        primaryStage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void download(List<String> records, String path) {
        DiscogsService discogsApi;
        try {
            discogsApi = new DiscogsService();

            List<DeklonedRecord> releases = new ArrayList<>();
            List<String> notExistent = new ArrayList<>();

            for (String record : records) {
                DiscogsRelease release = discogsApi.downloadRelease(record);
                if (release != null) {
                    releases.add(DeklonedUtils.convertReleaseToRecord(release));
                } else {
                    notExistent.add(record);
                }
            }

            CSVExporter csvExporter = new CSVExporter(new File(path));
            csvExporter.export(releases);

            if (!notExistent.isEmpty()) {
                Dialogs.create()
                        .owner(stage)
                        .title("Warning")
                        .masthead(null)
                        .message("The following releases seems to be not existent: \n" + notExistent)
                        .showWarning();
            }

            if (!releases.isEmpty()) {
                Dialogs.create()
                        .owner(stage)
                        .title("Success")
                        .masthead(null)
                        .message("The releases are successfully downloaded in:\n" + path)
                        .showInformation();

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .masthead(null)
                    .message("An error occurred while processing the request:" + ex.getMessage())
                    .showException(ex);

        }
    }
}
