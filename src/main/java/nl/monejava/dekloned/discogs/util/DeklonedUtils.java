/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.monejava.dekloned.discogs.util;

import nl.monejava.dekloned.discogs.DeklonedRecord;
import nl.monejava.dekloned.discogs.entities.Artist;
import nl.monejava.dekloned.discogs.entities.DiscogsRelease;
import nl.monejava.dekloned.discogs.entities.Format;
import nl.monejava.dekloned.discogs.entities.Tracklist;

/**
 *
 * @author Simone Rondelli - mone.java[at]gmail.com
 */
public class DeklonedUtils {

    public static final String DESCRIPTION_PATTERN = "<li><a href=\"%s\" title=\"%s\">%s</a></li>";

    public static final String META_DESC_PATTERN = "Buy %s ‎– %s on %s at Dekloned Records. Fair prices, fast and reliable shipping!";

    public static String getModel(DiscogsRelease release) {
        return "" + release.getId();
    }

    public static String getWeight(DiscogsRelease release) {
        return "" + release.getEstimatedWeight();
    }

    public static String getName(DiscogsRelease release) {
        return "" + release.getTitle();
    }

    public static String getMetaDescription(DiscogsRelease release) {
        String format = "";

        if (!release.getFormats().isEmpty()) {
            format += release.getFormats().get(0).getName();
        }

        return String.format(META_DESC_PATTERN, getProductTags(release), release.getTitle(), format);
    }

    public static String getMetaKeyword(DiscogsRelease release) {
        String meta = getCatno(release) + ",";

        if (!release.getStyles().isEmpty()) {
            meta += String.join(",", release.getStyles()) + ",";
        }

        for (Tracklist track : release.getTracklist()) {
            meta += track.getTitle() + ",";
        }

        for (Format format : release.getFormats()) {
            meta += format.getName();
            break;
        }

        return meta;
    }

    public static String getManufacturer(DiscogsRelease release) {
        if (!release.getLabels().isEmpty()) {
            return release.getLabels().get(0).getName();
        }
        return "";
    }

    public static String getAdditionalImage(DiscogsRelease release) {
        String images = "";
        if (release.getImages().size() > 1) {
            String catno = getCatnoAsPath(release);

            for (int i = 1; i < release.getImages().size(); i++) {
                images += "records/" + catno + "/" + (i + 1) + ".jpg";

                if (i < release.getImages().size() - 1) {
                    images += ", ";
                }
            }
        }
        return images;
    }

    public static String getImage(DiscogsRelease release) {
        if (!release.getImages().isEmpty()) {
            String catno = getCatnoAsPath(release);
            return "records/" + catno + "/1.jpg";
        }
        return "";
    }

    public static String getProductTags(DiscogsRelease release) {
        String productTags = "";

        for (Artist art : release.getArtists()) {
            String join = art.getJoin();

            if (!join.isEmpty()) {
                join = ", ";
            }

            String name = art.getName();

            name = name.replaceAll("[(]*[0-9][)]", "");
            name = name.replaceAll("\\*", "");
            name = name.trim();

            productTags += name + join;
        }

        return productTags;
    }

    public static String getFormat(DiscogsRelease release) {
        String size = null;

        int num = 0;

        for (Format format : release.getFormats()) {
            String type = format.getName();

            if (type.equalsIgnoreCase("vinyl")) {
                size = "12\"";
            }
            num++;
        }

        if (size != null) {
            return num + " x " + size;
        } else {
            return "";
        }
    }

    public static String getDescription(DiscogsRelease release) {

        String catno = getCatnoAsPath(release);
        String description = "";

        int i = 1;
        for (Tracklist track : release.getTracklist()) {
            String path = "records/" + catno + "/mp3/" + (i++) + ".mp3";
            String position = track.getPosition();
            String title = track.getTitle();
            description += String.format(DESCRIPTION_PATTERN, path, title, position);
        }

        return description;
    }

    public static String getCatnoAsPath(DiscogsRelease release) {
        return getCatno(release).trim().toLowerCase().replace(" ", "-");
    }

    public static String getCatno(DiscogsRelease release) {
        if (!release.getLabels().isEmpty()) {
            return release.getLabels().get(0).getCatno();
        }
        return "";
    }

    public static String getMinimum(DiscogsRelease release) {
        return "" + 1;
    }

    public static String getSubtract(DiscogsRelease release) {
        return "" + 1;
    }

    public static String getStatus(DiscogsRelease release) {
        return "" + 1;
    }

    public static String getStockStatus(DiscogsRelease release) {
        return "Out of Stock";
    }

    public static String getShipping(DiscogsRelease release) {
        return "" + 1;
    }

    public static String getSaxClass(DiscogsRelease release) {
        return "Belasting";
    }

    public static String getSeoKeyWord(DiscogsRelease release) {
        return getCatnoAsPath(release);
    }

    public static DeklonedRecord convertReleaseToRecord(DiscogsRelease release) {
        String model = "" + DeklonedUtils.getModel(release);
        System.out.println("Model: " + model);

        String name = DeklonedUtils.getName(release);
        System.out.println("Name: " + name);

        String weight = DeklonedUtils.getWeight(release);
        System.out.println("Weight: " + weight);

        String catno = DeklonedUtils.getCatno(release);
        System.out.println("Catno: " + catno);

        String format = DeklonedUtils.getFormat(release);
        System.out.println("Format: " + format);

        String productTag = DeklonedUtils.getProductTags(release);
        System.out.println("Product Tags: " + productTag);

        String image = DeklonedUtils.getImage(release);
        System.out.println("Image: " + image);

        String additionalImage = DeklonedUtils.getAdditionalImage(release);
        System.out.println("Add Images: " + additionalImage);

        String manufacturer = DeklonedUtils.getManufacturer(release);
        System.out.println("Manufacturer: " + manufacturer);

        String meta = DeklonedUtils.getMetaKeyword(release);
        System.out.println("Meta: " + meta);

        String metaDesc = DeklonedUtils.getMetaDescription(release);
        System.out.println("Meta desc: " + metaDesc);

        String description = DeklonedUtils.getDescription(release);
        System.out.println("Description: " + description);

        DeklonedRecord record = new DeklonedRecord();
        record.setAdditionalImage(additionalImage);
        record.setCatno(catno);
        record.setDescription(description);
        record.setFormat(format);
        record.setImage(image);
        record.setManufacturer(manufacturer);
        record.setMeta(meta);
        record.setMetaDesc(metaDesc);
        record.setModel(model);
        record.setName(name);
        record.setProductTag(productTag);
        record.getWeight();
        
        return record;
    }

}
