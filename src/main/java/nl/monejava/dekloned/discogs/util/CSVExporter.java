/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.monejava.dekloned.discogs.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import nl.monejava.dekloned.discogs.DeklonedRecord;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.quote.AlwaysQuoteMode;

/**
 *
 * @author Simone Rondelli - mone.java[at]gmail.com
 */
public class CSVExporter {

    private final File dest;

    public CSVExporter(File dest) {
        this.dest = dest;
    }

    public void export(List<DeklonedRecord> records) throws IOException {
        CsvPreference prefs = new CsvPreference.Builder('"', ';', "\n")
                .useQuoteMode(new AlwaysQuoteMode()).build();

        CellProcessor[] processors = new CellProcessor[18];

        for (int i = 0; i < processors.length; i++) {
            processors[i] = new Optional();
        }

        try (CsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(dest), prefs)) {
            String[] header = CsvMapping.getFieldsMapping(DeklonedRecord.class);
            beanWriter.writeHeader(header);
            for (DeklonedRecord record : records) {
                beanWriter.write(record, header, processors);
            }
        }
    }

}
