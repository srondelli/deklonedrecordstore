package nl.monejava.dekloned.discogs.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

public class CsvMapping {

    /**
     * Get DTO fields using Reflection.
     *
     * @param dtoType DTO Class
     * @return
     */
    public static <T> String[] getFieldsMapping(Class<T> dtoType) {
        List<String> fields = new ArrayList<>();
        for (Field f : dtoType.getDeclaredFields()) {
            fields.add(f.getName());
        }
        return fields.toArray(new String[fields.size()]);
    }

    /**
     * Returns Cell Processor for fields in the proper order. The type of the
     * field is taken through Reflection
     *
     * @param dtoType DTO Class
     * @param fields DTO fields order
     * @return Cell Processors
     *
     * @throws NoSuchFieldException
     * @throws SecurityException
     */
    public static <T> CellProcessor[] getFieldsCellProcessor(Class<T> dtoType, String[] fields) throws NoSuchFieldException, SecurityException {
        CellProcessor[] processors = new CellProcessor[fields.length];

        for (int i = 0; i < fields.length; i++) {
            Class<?> currentFieldType = dtoType.getDeclaredField(fields[i]).getType();

            if (currentFieldType == String.class) {
                processors[i] = new NotNull();
            } else if (currentFieldType == BigDecimal.class) {
                processors[i] = new NotNull(new ParseBigDecimal(DecimalFormatSymbols.getInstance(Locale.ITALIAN)));
            } else if (currentFieldType == Long.class) {
                processors[i] = new NotNull(new ParseLong());
            } else if (currentFieldType == Integer.class) {
                processors[i] = new NotNull(new ParseInt());
            } else if (currentFieldType == Double.class) {
                processors[i] = new NotNull(new ParseDouble());
            } else {
                throw new IllegalStateException(String.format("Il campo %s del DTO %s ha un tipo non riconosciuto -> %s", fields[i], dtoType.getName(),
                        currentFieldType.getName()));
            }
        }
        return processors;
    }
}
